# Datasets and R source code of article Depeux et al. (2022)

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/) [![DOI:10.5281/zenodo.7496837](https://zenodo.org/badge/DOI/10.5281/zenodo.7496837.svg)](https://doi.org/10.5281/zenodo.7496837)

R source code of manuscript by Depeux C, Branger A, Moulignier T, Moreau J, Lemaître J-F, Dechaume-Moncharmont F-X, Laverre T, Pauhlac H, Gaillard J-M, Beltran-Bech S (2022) Deleterious effects of thermal and water stresses on life history and physiology: a case study on woodlouse. **bioRxiv**, 2022.09.26.509512 https://doi.org/10.1101/2022.09.26.509512. 

This preprint has bee peer-reviewed and recommended by **Peer Community in Ecology**:  Belsare A (2022) An experimental approach for understanding how terrestrial isopods respond to environmental stressors. Peer Community in Ecology, 100506. https://doi.org/10.24072/pci.ecology.100506

This repository is an archive of the working versions of the project. The final files cited in the article are to be found on **Zenodo** (DOI: 10.5281/zenodo.7496837, https://doi.org/10.5281/zenodo.7496837) for long-term archiving. 

	Project
	  ├── README
	  ├── R_script.R
	  └── datasets
	         ├── mass_temperature.csv
	         ├── mass_moisture.csv
	         ├── survival_temperature.csv
	         ├── survival_moisture.csv
	         ├── reproduction_temperature.csv
	         ├── reproduction_moisture.csv
	         ├── biomarkers_temperatures.csv
	         └── biomarkers_moisture.csv
	  
**R source code**: Complete analyses are presented in one single file `R_script.R`. See comments in the script for additional information.
